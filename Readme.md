
# To test kafka using a Docker image

https://hub.docker.com/r/bitnami/kafka/

Execute in a terminal

```
$ curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-kafka/master/docker-compose.yml > docker-compose.yml
$ docker-compose up -d
```
The we will have this

```
docker ps --all
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                  NAMES
d47d29cd6a51   bitnami/kafka:2         "/opt/bitnami/script…"   4 minutes ago    Up 4 minutes    9092/tcp, 0.0.0.0:9093->9093/tcp                       kafka_kafka_1
bd3cc88185e2   bitnami/zookeeper:3.7   "/opt/bitnami/script…"   17 minutes ago   Up 17 minutes   2888/tcp, 3888/tcp, 0.0.0.0:2181->2181/tcp, 8080/tcp   kafka_zookeeper_1
```
To connet to that Kafka server

```
kafka-topics --bootstrap-server localhost:9093 --list
```