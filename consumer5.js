const { Kafka } = require('kafkajs')

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092']
})

const consumer = kafka.consumer({ groupId: 'Group2' })

consumer.connect()
consumer.subscribe({ topic: 'measures', fromBeginning: false })

consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
        console.log({
            partition,
            value: message.value.toString(),
        })
    },
})