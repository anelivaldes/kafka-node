const { Kafka } = require('kafkajs')

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092']
})

const producer = kafka.producer()
producer.connect()
let a = 0;
setInterval(() => {
    const v = a+=1;
    producer.send({
        topic: 'measures',
        messages: [
            { value: v.toString()},
        ],
    }).then(console.log, console.error)
}, 1000);
