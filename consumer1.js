const { Kafka } = require('kafkajs')

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092']
})

const consumer = kafka.consumer({ groupId: 'Group1' })

consumer.connect()
consumer.subscribe({ topic: 'measures', fromBeginning: true })

consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
        console.log({
            partition,
            value: message.value.toString(),
        })
    },
})